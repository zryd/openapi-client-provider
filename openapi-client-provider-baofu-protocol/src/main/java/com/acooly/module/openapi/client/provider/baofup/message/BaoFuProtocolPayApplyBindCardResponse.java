package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPResponse;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/4/12 15:59
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_PAY_APPLY_BIND_CARD,type = ApiMessageType.Response)
public class BaoFuProtocolPayApplyBindCardResponse extends BaoFuPResponse {

    /**
     * 业务返回码
     */
    @NotBlank
    @BaoFuPAlias(value = "biz_resp_code")
    private String bizRespCode;

    /**
     * 业务返回说明
     */
    @NotBlank
    @BaoFuPAlias(value = "biz_resp_msg")
    private String bizRespMsg;

    /**
     * 预签约唯一码
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @BaoFuPAlias(value = "unique_code",isDecode = true)
    private String uniqueCode;
}
