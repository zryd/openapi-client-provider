package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPRequest;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/4/13 14:29
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_PAY_CONFIRM_BIND_CARD,type = ApiMessageType.Request)
public class BaoFuProtocolPayConfirmBindCardRequest extends BaoFuPRequest {

    /**
     * 与签约申请信息(不用传,分开传入一下字段组建会自动拼装)
     * 格式：预签约唯一码|短信验证码（顺序不能变）
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @BaoFuPAlias(value = "unique_code",isEncrypt = true)
    private String applyCode;

    /**
     * 预签约唯一码
     */
    @NotBlank
    private String uniqueCode;
    /**
     * 短息验证码
     */
    @NotBlank
    private String smsCode;

    /**
     * 组装请求信息
     * @return
     */
    public String getApplyCode() {
        return String.format("%s|%s", getUniqueCode(), getSmsCode());
    }
}
