package com.acooly.module.openapi.client.provider.bosc.message.response;

import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponseDomain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VirAcctInResponse extends BoscResponseDomain {

	/**
	 * 银行返回流水号
	 */
	private String flowNo;

}
