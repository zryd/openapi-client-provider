package com.acooly.module.openapi.client.provider.cmb.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbApiMsgInfo;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbRequest;
import com.acooly.module.openapi.client.provider.cmb.enums.CmbServiceEnum;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@CmbApiMsgInfo(service = CmbServiceEnum.cmbDirectPay, type = ApiMessageType.Request)
public class HthDirectPayRequest extends CmbRequest {

    /**
     * 业务订单号
     */
    private String merchOrderNo;

    /**
     * 代发金额
     */
    private String amount;

    /**
     * 收款行联行号 (对公，跨行必填项)
     */
    private String recBankLasalle;

    /**
     * 收款账号所在省份.
     */
    private String recBankProvince;

    /**
     * 收款账号所在城市
     */
    private String recBankCity;

    /**
     * 收款账号所在城市.
     */
    private String recBankAddress;
    /**
     * 客户真实姓名
     */
    @NotBlank
    private String recRealName;

    /**
     * 收款客户手机号
     */
    @NotBlank
    private String recMobileNo;

    /**
     * 收款客户身份证号码
     */
    private String recCertNo;

    /**
     * 银行编码
     */
    @NotBlank
    private String recBankCode;

    /**
     * 银行名称
     */
    @NotBlank
    private String recBankName;

    /**
     * 银行卡号
     */
    @NotBlank
    private String recBankCardNo;

    /**
     * 付款用户所在银行的ID
     */
    private String payBankCode;

    /**
     * 付款用户的银行卡号 (必填项)
     */
    @NotBlank
    private String payAccountNo;

    /**
     * 付款用户姓名 (必填项)
     */
    @NotBlank
    private String payAccountName;

    /**
     * 付款行名称 (必填项)
     */
    @NotBlank
    private String payBankName;

    /**
     * 收款方银行所在省级名称
     */
    private String payBankProvince;

    /**
     * 收款方银行所在市级名称
     */
    private String payBankCity;

    /**
     * 收款方银行所在地址
     */
    private String payBankAddress;

    /**
     * userid
     */
    private String userId;

    /**
     * 对公对私标志 如：Y对公 N对私
     */
    private String publicTag = "N";

    /**
     * 银行卡类型 如：DEBIT借记卡, CREDIT 贷记卡
     */
    private String bankCardType;

    /**
     * 代扣冲退转提现标志
     */
    private String withdrawFlag = "N";

    /**
     * 转账用途描述
     */
    private String transUsage;

    /**
     * 是否行内转账
     */
    private String interbank;

    /**
     * 是否同城转账
     */
    private String local;

    /**
     * 支付方式（超级网银跨行支付必填SUPER）
     */
    private String pmtmode;


    /**
     * 银行订单号
     */
    private String bankOrderNo;

}
