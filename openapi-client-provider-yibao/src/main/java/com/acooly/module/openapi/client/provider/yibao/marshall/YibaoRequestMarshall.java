/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 16:11 创建
 */
package com.acooly.module.openapi.client.provider.yibao.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 同步请求报文组装
 *
 * @author zhangpu 2018-1-23 16:11
 */
@Slf4j
@Component
public class YibaoRequestMarshall extends YibaoAbstractMarshall implements ApiMarshal<String, YibaoRequest> {

    @Override
    public String marshal(YibaoRequest source) {
        return doMarshall(source);
    }


}
