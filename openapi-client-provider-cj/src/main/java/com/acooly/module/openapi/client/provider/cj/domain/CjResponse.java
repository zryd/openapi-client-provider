/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.cj.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 *
 * @author zhangpu
 */
@Getter
@Setter
public class CjResponse extends CjApiMessage {

	/**
	 * 订单号
	 */
	String bizOrderNo;

	/**
	 * 批次号
	 */
	String batchNo;

	/**
	 * 金额 (整数，单位分)
	 */
	private String amount;

	/**
	 * 接口状态码
	 * 成功、失败返回
	 */
	private String retCode;

	/**
	 * 接口状态描述
	 * 成功、失败返回
	 */
	private String errMsg;

	String status;
	String detail;
	String errCode;
	String errDetail;
}
