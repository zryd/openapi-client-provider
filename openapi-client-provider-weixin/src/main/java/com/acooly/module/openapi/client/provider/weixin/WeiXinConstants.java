package com.acooly.module.openapi.client.provider.weixin;

/**
 * @author jiandao
 */
public class WeixinConstants {
    
    public static String SUCCESS_CODE = "SUCCESS";
    
    public static String TRADE_STATE_PAYING = "USERPAYING";
    
    public static String TRADE_STATE_NOT_PAY = "NOTPAY";
}
