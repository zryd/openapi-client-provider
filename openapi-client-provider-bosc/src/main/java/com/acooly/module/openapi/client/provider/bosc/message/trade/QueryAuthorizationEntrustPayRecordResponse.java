package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscEntrustedAuthorizeStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscEntrustedTypeEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_AUTHORIZATION_ENTRUST_PAY_RECORD, type = ApiMessageType.Response)
public class QueryAuthorizationEntrustPayRecordResponse extends BoscResponse {
	/**
	 * 委托支付授权审核状态，AUDIT(待审核),FAIL（授权失败）,PASSED(授权成功)
	 */
	@NotNull
	private BoscEntrustedAuthorizeStatusEnum authorizeStatus;
	/**
	 * 借款方平台会员编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String borrowPlatformUserNo;
	/**
	 * 受托方平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String entrustedPlatformUserNo;
	/**
	 * 委托支付授权请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 标的号
	 */
	@NotEmpty
	@Size(max = 50)
	private String projectNo;
	/**
	 * 受托方类型，枚举值 PERSONAL（个人），ENTERPRISE（企业）
	 */
	@NotNull
	private BoscEntrustedTypeEnum entrustedType;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date createTime;
	/**
	 * 完成时间，委托授权请求状态为 SUCCESS 或 FAIL 时返回
	 */
	private Date completedTime;
	
	public BoscEntrustedAuthorizeStatusEnum getAuthorizeStatus () {
		return authorizeStatus;
	}
	
	public void setAuthorizeStatus (
			BoscEntrustedAuthorizeStatusEnum authorizeStatus) {
		this.authorizeStatus = authorizeStatus;
	}
	
	public String getBorrowPlatformUserNo () {
		return borrowPlatformUserNo;
	}
	
	public void setBorrowPlatformUserNo (String borrowPlatformUserNo) {
		this.borrowPlatformUserNo = borrowPlatformUserNo;
	}
	
	public String getEntrustedPlatformUserNo () {
		return entrustedPlatformUserNo;
	}
	
	public void setEntrustedPlatformUserNo (String entrustedPlatformUserNo) {
		this.entrustedPlatformUserNo = entrustedPlatformUserNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
	
	public BoscEntrustedTypeEnum getEntrustedType () {
		return entrustedType;
	}
	
	public void setEntrustedType (BoscEntrustedTypeEnum entrustedType) {
		this.entrustedType = entrustedType;
	}
	
	public Date getCreateTime () {
		return createTime;
	}
	
	public void setCreateTime (Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getCompletedTime () {
		return completedTime;
	}
	
	public void setCompletedTime (Date completedTime) {
		this.completedTime = completedTime;
	}
}