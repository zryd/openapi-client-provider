package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import org.hibernate.validator.constraints.NotEmpty;

@ApiMsgInfo(service = BoscServiceNameEnum.USER_AUTO_PRE_TRANSACTION,type = ApiMessageType.Response)
public class UserAutoPreTransactionResponse extends BoscResponse {
	/**
	 * 见【预处理业务类型】,若传入关联请求流水号，则业务类型需要相对应
	 */
	@NotEmpty
	private BoscBizTypeEnum bizType;
	
	public BoscBizTypeEnum getBizType () {
		return bizType;
	}
	
	public void setBizType (BoscBizTypeEnum bizType) {
		this.bizType = bizType;
	}
}