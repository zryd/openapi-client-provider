/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.newyl.marshall;


import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlRequest;

import org.springframework.stereotype.Service;

/**
 * @author fufeng
 */
@Service
public class NewYlRequestMarshall extends NewYlMarshallSupport implements ApiMarshal<String, NewYlRequest> {

    @Override
    public String marshal(NewYlRequest source) {
        beforeMarshall(source);
        return doMarshall(source);
    }

}
