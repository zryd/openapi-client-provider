/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by zhike
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.baofu.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike Date: 2017-03-30 15:12:06
 */
public enum BaoFuServiceVersionEnum implements Messageable {
  VERSION_4000("4.0.0.0", "版本为4.0.0.0的服务"),
  VERSION_4002("4.0.0.2", "版本为4.0.0.2的服务"),
  VERSION_400("4.0.0", "版本为4.0.0的服务"),
  VERSION_40("4.0", "版本为4.0的服务"),
  ;

  private final String code;
  private final String message;

  private BaoFuServiceVersionEnum(String code, String message) {
    this.code = code;
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public String code() {
    return code;
  }

  public String message() {
    return message;
  }

  public static Map<String, String> mapping() {
    Map<String, String> map = new LinkedHashMap<String, String>();
    for (BaoFuServiceVersionEnum type : values()) {
      map.put(type.getCode(), type.getMessage());
    }
    return map;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param code 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
   */
  public static BaoFuServiceVersionEnum find(String code) {
    for (BaoFuServiceVersionEnum status : values()) {
      if (status.getCode().equals(code)) {
        return status;
      }
    }
    return null;
  }

  /**
   * 获取全部枚举值。
   *
   * @return 全部枚举值。
   */
  public static List<BaoFuServiceVersionEnum> getAll() {
    List<BaoFuServiceVersionEnum> list = new ArrayList<BaoFuServiceVersionEnum>();
    for (BaoFuServiceVersionEnum status : values()) {
      list.add(status);
    }
    return list;
  }

  /**
   * 获取全部枚举值码。
   *
   * @return 全部枚举值码。
   */
  public static List<String> getAllCode() {
    List<String> list = new ArrayList<String>();
    for (BaoFuServiceVersionEnum status : values()) {
      list.add(status.code());
    }
    return list;
  }
}
