/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.baofu.marshall;

import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.baofu.BaoFuConstants;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.message.BaoFuAccountBalanceQueryRequest;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuRequestParams;
import com.acooly.module.openapi.client.provider.baofu.utils.BaoFuSecurityUtil;
import com.acooly.module.openapi.client.provider.baofu.utils.StringHelper;
import com.acooly.module.openapi.client.provider.baofu.utils.XmlUtils;
import com.acooly.module.safety.Safes;
import com.acooly.module.safety.signature.SignTypeEnum;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author zhike
 */
@Service
@Slf4j
public class BaoFuRequestMarshall extends BaoFuMarshallSupport implements ApiMarshal<String, BaoFuRequest> {

    @Override
    public String marshal(BaoFuRequest source) {
        beforeMarshall(source);
        SortedMap<String, String> requestDataMap = doMarshall(source);
        String requestStr = StringHelper.getRequestStr(requestDataMap);
        return requestStr;
    }

    public String withdrawMarshal(BaoFuRequest source) {
        doVerifyParam(source);
        beforeMarshall(source);
        String signData = XmlUtils.toXml(source);
        log.info("签名字符串：{}",signData);
        try {
            signData = BaoFuSecurityUtil.Base64Encode(signData);
        }catch (UnsupportedEncodingException ue) {
            log.info("Base64加密字符串失败：{}",ue.getMessage());
            throw new ApiClientException(ue.getMessage());
        }
        source.setDataContent(doSign(signData,source,false));
        BaoFuRequestParams baoFuRequest = new BaoFuRequestParams();
        baoFuRequest.setVersion(source.getVersion());
        baoFuRequest.setDataContent(source.getDataContent());
        baoFuRequest.setMemberId(source.getMemberId());
        baoFuRequest.setTerminalId(source.getTerminalId());
        String requestStr = StringHelper.getRequestStr(getRequestDataMap(baoFuRequest));
        return requestStr;
    }

    public String accountBalanceQueryMarshal(BaoFuRequest source) {
        doVerifyParam(source);
        beforeMarshall(source);
        BaoFuAccountBalanceQueryRequest request = (BaoFuAccountBalanceQueryRequest)source;
        String md5Key = openAPIClientBaoFuProperties.getMd5Key();
        LinkedHashMap<String,String> plain = Maps.newLinkedHashMap();
        plain.put("member_id",request.getMemberId());
        plain.put("terminal_id",request.getTerminalId());
        plain.put("return_type",request.getReturnType());
        plain.put("trans_code",BaoFuServiceEnum.ACCOUNT_BALANCE_QUERY.getKey());
        plain.put("version",request.getVersion());
        plain.put("account_type",request.getAccountType());
        String signStr = Safes.getSigner(BaoFuConstants.MD5_SIGNER_TYPE).sign(plain,md5Key).toUpperCase();
        plain.put("sign",signStr);
        SortedMap<String,String> sort=new TreeMap(plain);
        String requestStr = StringHelper.getRequestStr(sort);
        return requestStr;
    }

}
