package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuWithdrawQueryRequestInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author zhike 2018/2/2 18:07
 */
@Getter
@Setter
@XStreamAlias("trans_content")
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.WITHDRAW_QUERY,type = ApiMessageType.Request)
public class BaoFuWithdrawQueryRequest extends BaoFuRequest{

    @XStreamAlias("trans_reqDatas")
    private List<BaoFuWithdrawQueryRequestInfo> withdrawQueryInfos;

    @Override
    public void doCheck() {
        super.doCheck();
        if(withdrawQueryInfos == null) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(),"批量提交提现查询不能为空");
        }else if(withdrawQueryInfos.size() > 5) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(),"批量提交提现查询数量不能一次超过5条");
        }
        for(BaoFuWithdrawQueryRequestInfo withdrawQueryInfo:withdrawQueryInfos) {
            Validators.assertJSR303(withdrawQueryInfo);
        }
    }
}
