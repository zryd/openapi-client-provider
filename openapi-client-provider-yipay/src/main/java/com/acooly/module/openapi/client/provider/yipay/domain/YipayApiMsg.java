/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-26 02:01 创建
 */
package com.acooly.module.openapi.client.provider.yipay.domain;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zhangpu 2017-09-26 02:01
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface YipayApiMsg {
    /**
     * 报文类型
     *
     * @return
     */
    ApiMessageType type() default ApiMessageType.Request;

    /**
     * 服务名称
     *
     * @return
     */
    YipayServiceNameEnum service() default YipayServiceNameEnum.UNKNOWN;

}
