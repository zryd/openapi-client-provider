/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.allinpay.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

/** @author zhike */
public class AllinpayApiMessage implements ApiMessage {

  /** 接口名称 */
  @XStreamOmitField
  private String service;

  /** 请求url */
  @XStreamOmitField
  private String gatewayUrl;

  public String getSignStr() {
    return null;
  }

  public void doCheck() {}

  @Override
  public String getService() {
    return service;
  }

  @Override
  public String getPartner() {
    return null;
  }

  @Override
  public String toString() {
    return ToString.toString(this);
  }

  public String getGatewayUrl() {
    return gatewayUrl;
  }

  public void setGatewayUrl(String gatewayUrl) {
    this.gatewayUrl = gatewayUrl;
  }

    public void setService(String service) {
        this.service = service;
    }
}
