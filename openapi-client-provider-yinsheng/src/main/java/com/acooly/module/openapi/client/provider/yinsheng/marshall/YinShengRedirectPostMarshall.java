package com.acooly.module.openapi.client.provider.yinsheng.marshall;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.yinsheng.YinShengConstants;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengRequest;
import com.acooly.module.openapi.client.provider.yinsheng.utils.SignUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
@Slf4j
public class YinShengRedirectPostMarshall extends YinShengMarshallSupport implements ApiMarshal<PostRedirect, YinShengRequest> {

    @Override
    public PostRedirect marshal(YinShengRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        if(Strings.isBlank(source.getGatewayUrl())) {
            source.setGatewayUrl(getProperties().getGatewayUrl());
        }
        if(Strings.isBlank(source.getPartner_id())) {
            source.setPartner_id(getProperties().getPartnerId());
        }
        postRedirect.setRedirectUrl(source.getGatewayUrl());
        postRedirect.setFormDatas(doMarshall(source));
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }

    /**
     * 网关支付特殊处理
     * @param source
     * @return
     */
    protected Map<String, String> doMarshall(YinShengRequest source) {
        doVerifyParam(source);
        Map<String, String> requestDataMap = getRequestDataMap(source);
        String signContent = SignUtils.getSignContent(requestDataMap);
        log.debug("待签字符串:{}", signContent);
        source.setSign(doSign(signContent, source));
        requestDataMap.put(YinShengConstants.SIGN, source.getSign());
        log.info("请求报文: {}", requestDataMap);
        return requestDataMap;
    }
}
