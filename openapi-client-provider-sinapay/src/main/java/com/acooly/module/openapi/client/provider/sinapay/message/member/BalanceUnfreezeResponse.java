package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/7/10 14:13
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.BALANCE_UNFREEZE, type = ApiMessageType.Response)
public class BalanceUnfreezeResponse extends SinapayResponse {

}
