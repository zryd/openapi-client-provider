package com.acooly.module.openapi.client.provider.sinapay.message.trade.dto;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import com.acooly.module.openapi.client.provider.sinapay.message.Dtoable;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 交易参数
 *
 * @author xiaohong
 * @create 2018-07-18 15:49
 **/
@Getter
@Setter
@ApiDto
public class TradeParam implements Dtoable {
    /**
     * 交易订单号
     *
     * 商户网站交易订单号，商户内部保证唯一
     */
    @NotEmpty
    @Size(max = 32)
    @ItemOrder(0)
    private String outTradeNo = Ids.oid();

    /**
     * 收款方式
     *
     * 格式：收款方式^扩展。扩展信息内容以“，”分隔，收款方式扩展详见附录
     */
    @NotEmpty
    @Size(max = 1000)
    private String collectMethod;

    /**
     * 金额
     */
    @NotNull
    @MoneyConstraint
    private Money amount;

    /**
     * 摘要
     *
     * 交易内容摘要
     */
    @NotEmpty
    @Size(max = 64)
    private String summary;

    /**
     * 扩展信息
     *
     * 业务扩展信息，参数格式：参数名1^参数值1|参数名2^参数值2|……
     */
    @Size(max = 200)
    private String extendParam;

    /**
     * 标的号
     *
     * 代付到提现卡时 非白名单商户必传（产品码301008）
     */
    @Size(max = 64)
    @NotEmpty
    private String goodsId;

    /**
     * 债权变动明细列表
     *
     * 参考: 债权变动明细参数，当放款给借款人或还款给投资人场景时需要 参数间用“^”分割，条目间用“|”分割 恒丰存管商户非空
     */
    private String debtChangeDetail;

    /**
     * 交易关联号
     *
     * 商户交易关联号，用于代收代付交易关联
     */
    @NotEmpty
    @Size(max = 32)
    private String tradeRelatedNo;
}
