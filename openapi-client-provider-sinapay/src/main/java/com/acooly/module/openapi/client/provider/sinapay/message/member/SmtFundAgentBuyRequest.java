package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 15:41
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.SMT_FUND_AGENT_BUY, type = ApiMessageType.Request)
public class SmtFundAgentBuyRequest extends SinapayRequest {
    /**
     *用户标识信息
     * 商户系统用户ID(字母或数字)
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "identity_id")
    private String identityId;

    /**
     *用户标识类型
     * ID的类型，目前只包括UID
     */
    @NotEmpty
    @Size(max = 16)
    @ApiItem(value = "identity_type")
    private String identityType = "UID";

    /**
     *经办人姓名
     * 密文，使用新浪支付RSA公钥加密。明文长度：50
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "agent_name",securet = true)
    private String agentName;

    /**
     *经办人身份证
     * 密文，使用新浪支付RSA公钥加密。明文长度：30
     */
    @NotEmpty
    @Size(max = 18)
    @ApiItem(value = "license_no",securet = true)
    private String licenseNo;

    /**
     *大陆身份证类型：ID
     */
    @NotEmpty
    @Size(max = 16)
    @ApiItem(value = "license_type_code")
    private String licenseTypeCode = "ID";

    /**
     *经办人手机号
     * 密文，使用新浪支付RSA公钥加密。明文长度：30
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "agent_mobile",securet = true)
    private String agentMobile;

    /**
     *经办人邮箱
     */
    @Size(max = 32)
    @ApiItem(value = "email")
    private String email;

    /**
     *请求者IP
     * 用户在商户平台操作时候的IP地址，公网IP，不是内网IP
     * 用于风控校验，请填写用户真实IP，否则容易风控拦截
     */
    @NotEmpty
    @Size(max = 50)
    @ApiItem(value = "client_ip")
    private String clientIp;


}
