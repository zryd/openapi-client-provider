/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月1日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 委托扣款重定向 请求
 * 
 * 该请求完成后，同步跳转回来的URL中会GET参数带入identify=
 * 
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.HANDLE_WITHHOLD_AUTHORITY, type = ApiMessageType.Request)
public class HandleWithholdAuthorityRequest extends SinapayRequest {

	/**
	 * 用户标识信息
	 */
	@NotEmpty
	@Size(max = 50)
	@ApiItem(value = "identity_id")
	private String identityId;

	/**
	 * 交易码
	 *
	 * 商户网站代收交易业务码，见附录
	 */
	@NotEmpty
	@Size(max = 16)
	@ApiItem(value = "identity_type")
	private String identityType = "UID";

	/**
	 * 单笔额度
	 * 
	 * 授权可设置的最小单笔额度，++为无限大
	 */
	@Size(max = 16)
	@ApiItem(value = "quota")
	private String quota = "++";

	/**
	 * 日限
	 * 
	 * 授权可设置的最小单笔额度，++为无限大
	 */
	@Size(max = 16)
	@ApiItem(value = "day_quota")
	private String dayQota = "++";

	/**
	 * 代扣授权类型白名单
	 * 
	 * 以逗号分隔，期望的授权类型，目前可用的授权类型为 ALL:银行卡授权(该授权包含账户授权) ACCOUNT,账户授权
	 */
	@Size(max = 200)
	@ApiItem(value = "auth_type_whitelist")
	private String authTypeWhitelist = "ACCOUNT";
}
