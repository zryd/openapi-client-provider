/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.SET_REAL_NAME, type = ApiMessageType.Request)
public class SetRealNameRequest extends SinapayRequest {

	/** 用户标识信息 */
	@Size(max = 50)
	@NotEmpty
	@ApiItem("identity_id")
	private String identityId;

	/** 用户标识类型 */
	@Size(max = 16)
	@NotEmpty
	@ApiItem("identity_type")
	private String identityType = "UID";

	/** 姓名 */
	@Size(max = 1024)
	@NotEmpty
	@ApiItem(value = "real_name", securet = true)
	private String realName;

	/** 证件类型 */
	@Size(max = 18)
	@NotEmpty
	@ApiItem(value = "cert_type")
	private String certType = "IC";

	/** 证件号码 */
	@Size(max = 18)
	@NotEmpty
	@ApiItem(value = "cert_no", securet = true)
	private String certNo;

	/** 是否需要钱包做实名认证，值为Y/N，默认Y。暂不开放外部自助实名认证 */
	@Size(max = 1)
	@ApiItem("need_confirm")
	private String needConfirm;
	
	/** 用户IP/运营商IP */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "client_ip")
	private String clientIp;
	
	public SetRealNameRequest() {
		super();
	}

	/**
	 * @param identityId
	 * @param realName
	 * @param certNo
	 */
	public SetRealNameRequest(String identityId, String realName, String certNo, String clientIp) {
		super();
		this.identityId = identityId;
		this.realName = realName;
		this.certNo = certNo;
		this.clientIp = clientIp;
	}
}
