/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.fbank;

import com.acooly.core.utils.Strings;

/**
 * @author zhike 2017-09-17 17:49
 */
public class FbankConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String PROVIDER_NAME = "fbankProvider";

    public static final String SIGN_KEY = "signature";
    public static final String SIGN_TYPE = "MD5";
    public static final String SIGNER_TYPE = "fbankMd5";

    public static final String RESP_MESSAGE_KEY = "respMessage";
    public static final String PARTNER_KEY = "partnerId";


    public static String getCanonicalUrl(String domainUrl, String serviceName) {
        String serviceUrl = domainUrl;
        serviceUrl = Strings.removeEnd(serviceUrl, "/");
        if (!Strings.startsWith(serviceName, "/")) {
            serviceUrl = serviceUrl + "/" + serviceName;
        } else {
            serviceUrl = serviceUrl + serviceName;
        }
        return serviceUrl;
    }

}
