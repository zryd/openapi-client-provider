/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.fbank.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike Date: 2017-03-30 15:12:06
 */
public enum FbankServiceEnum implements Messageable {
    DEFAULT("default", "default", "默认"),
    TRADE_QUERY("/interface/trans/query", "service.trans.query", "交易查询"),
    TRADE_REFUND_QUERY("/interface/trans/refundQuery", "service.trans.refund.query", "退款查询"),
    PREPAY_API("/sdk/fumin/prepay/api", "prePayApi", "API支付"),
    TRADE_CANCEL("/interface/trans/cancel", "service.trans.cancel", "交易撤销"),
    TRADE_CLOSE("/interface/trans/close", "service.trans.close", "交易关闭"),
    TRADE_DOWNLOAD_FILE("/interface/trans/file", "service.trans.file", "对账文件下载"),
    TRADE_REFUND("/interface/trans/refund", "service.trans.refund", "退款"),

    ;

    private final String code;
    private final String key;
    private final String message;

    private FbankServiceEnum(String code, String key, String message) {
        this.code = code;
        this.message = message;
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public String getKey() {
        return key;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (FbankServiceEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static FbankServiceEnum find(String code) {
        for (FbankServiceEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param key 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
     */
    public static FbankServiceEnum findByKey(String key) {
        for (FbankServiceEnum status : values()) {
            if (status.getKey().equals(key)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<FbankServiceEnum> getAll() {
        List<FbankServiceEnum> list = new ArrayList<FbankServiceEnum>();
        for (FbankServiceEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (FbankServiceEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }
}
