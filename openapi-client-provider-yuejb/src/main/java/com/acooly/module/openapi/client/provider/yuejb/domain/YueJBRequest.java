package com.acooly.module.openapi.client.provider.yuejb.domain;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by ouwen@yiji.com} on 2017/11/6.
 */
@Data
public class YueJBRequest extends YueJBApiMessage {
    /**
     * 服务版本
     */
    private String version;
    /**
     * signType
     */
    @NotEmpty
    private String signType;
    /**
     * 签名字符串
     */
    @ApiItem(sign = false)
    @NotEmpty
    private String sign;
    /**
     * 商户订单号
     */
    @NotEmpty
    private String merchOrderNo;
    /**
     * 	会话参数
     */
    private String context;
    /**
     * 页面跳转地址
     */
    private String returnUrl;
    /**
     * 异步通知地址
     */
    private String notifyUrl;
}
