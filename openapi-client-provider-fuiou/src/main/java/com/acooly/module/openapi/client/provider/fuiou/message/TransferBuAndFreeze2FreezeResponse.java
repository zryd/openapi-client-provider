/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月5日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouResponse;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 冻结划拨冻结 响应报文
 * 
 * @author liuyuxiang
 */
public class TransferBuAndFreeze2FreezeResponse extends FuiouResponse {
	
	/** 请求付款冻结金额 */
	@XStreamAlias("amt")
	@FuiouAlias("amt")
	private String amt;
	
	/** 成功付款冻结金额 */
	@XStreamAlias("suc_amt")
	@FuiouAlias("suc_amt")
	private String sucAmt;

	public String getAmt() {
		return this.amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getSucAmt() {
		return this.sucAmt;
	}

	public void setSucAmt(String sucAmt) {
		this.sucAmt = sucAmt;
	}
}
