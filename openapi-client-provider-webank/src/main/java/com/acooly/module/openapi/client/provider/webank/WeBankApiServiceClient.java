/**
 * create by zhangpu
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.webank;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankNotify;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankRequest;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankResponse;
import com.acooly.module.openapi.client.provider.webank.marshall.WeBankNotifyUnmarshall;
import com.acooly.module.openapi.client.provider.webank.marshall.WeBankRedirectMarshall;
import com.acooly.module.openapi.client.provider.webank.marshall.WeBankRedirectPostMarshall;
import com.acooly.module.openapi.client.provider.webank.marshall.WeBankRequestMarshall;
import com.acooly.module.openapi.client.provider.webank.marshall.WeBankResponseUnmarshall;
import com.acooly.module.openapi.client.provider.webank.marshall.WeBankSignMarshall;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;

/**
 * 上海银行P2P存管ApiService执行器  外部业务禁止使用
 *
 * @author zhangpu
 */
@Slf4j
@Component("weBankApiServiceClient")
public class WeBankApiServiceClient
        extends AbstractApiServiceClient<WeBankRequest, WeBankResponse, WeBankNotify, WeBankNotify> {

    public static final String PROVIDER_NAME = "webank";

    @Resource(name = "weBankHttpTransport")
    private Transport transport;
    @Resource(name = "weBankRequestMarshall")
    private WeBankRequestMarshall requestMarshal;
    @Resource(name = "weBankRedirectMarshall")
    private WeBankRedirectMarshall redirectMarshal;
    @Resource(name = "weBankRedirectPostMarshall")
    private WeBankRedirectPostMarshall weBankRedirectPostMarshall;
    @Resource(name = "weBankSignMarshall")
    private WeBankSignMarshall signMarshall;

    @Autowired
    private WeBankResponseUnmarshall responseUnmarshal;

    @Autowired
    private WeBankNotifyUnmarshall notifyUnmarshal;

    @Autowired
    private WeBankProperties weBankProperties;

    @Override
    public WeBankResponse execute(WeBankRequest request) {
        try {
            beforeExecute(request);
            if(Strings.isBlank(request.getGatewayUrl())) {
                request.setGatewayUrl(weBankProperties.getGatewayUrl());
            }
            String url = request.getGatewayUrl();
            String requestMessage = getRequestMarshal().marshal(request);
            log.info("请求报文: {}", requestMessage);
            String responseMessage = getTransport().exchange(requestMessage, url);
            WeBankResponse t = getResponseUnmarshal().unmarshal(responseMessage, request.getService());
            afterExecute(t);
            return t;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    public WeBankNotify notice(HttpServletRequest request, String serviceKey) {
        try {
            Map<String,String> notifyData = getSignMarshall().getDateMap(request);
            WeBankNotify notify = getNoticeUnmarshal().unmarshal(notifyData, serviceKey);
            afterNotice(notify);
            return notify;
        } catch (ApiClientException oce) {
            log.warn("客户端:{}", oce.getMessage());
            throw oce;
        } catch (Exception e) {
            log.warn("内部错误:{}", e.getMessage());
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    public WeBankSignMarshall getSignMarshall() {
        return signMarshall;
    }

    @Override
    public PostRedirect redirectPost(WeBankRequest request) {
        try {
            beforeExecute(request);
            PostRedirect postRedirect = weBankRedirectPostMarshall.marshal(request);
            return postRedirect;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    protected ApiMarshal<String, WeBankRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<WeBankResponse, String> getResponseUnmarshal() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiUnmarshal<WeBankNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiMarshal<String, WeBankRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    protected ApiUnmarshal<WeBankNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }


}
