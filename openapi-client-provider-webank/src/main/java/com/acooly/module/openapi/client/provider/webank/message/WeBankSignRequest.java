package com.acooly.module.openapi.client.provider.webank.message;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankApiMsgInfo;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankRequest;
import com.acooly.module.openapi.client.provider.webank.enums.WeBankServiceEnum;
import com.acooly.module.openapi.client.provider.webank.message.dto.WeBankSignInfo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@WeBankApiMsgInfo(service = WeBankServiceEnum.WEBANK_SIGN, type = ApiMessageType.Request)
public class WeBankSignRequest extends WeBankRequest {

    /**
     * 签约业务信息
     */
    @ApiItem(value = "content")
    private WeBankSignInfo weBankSignInfo;
}
