package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.enums.WftBillTypeEnum;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2017/12/11 18:05
 * <p>
 * service接口类型参数值：（与传入的mch_id的类型对应）pay.bill.merchant：下载单个商户时的对账单
 * pay.bill.bigMerchant：下载大商户下所有子商户的对账单
 * pay.bill.agent：下载某渠道下所有商户的对账单
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.BILL_DOWNLOAD, type = ApiMessageType.Request)
public class WftBillDownloadRequest extends WftRequest {

    /**
     * 账单日期
     * 格式：yyyyMMdd(如：20150101)
     */
    @WftAlias(value = "bill_date")
    @Size(max = 8)
    @NotBlank
    private String billDate;

    /**
     * 账单类型
     * ALL，返回支付成功和转入退款的订单，默认值
     * SUCCESS，返回支付成功的订单
     * REFUND，返回转入退款的订单
     */
    @WftAlias(value = "bill_type")
    private String billType = WftBillTypeEnum.ALL.getCode();

    /**
     * 随机字符串
     */
    @WftAlias(value = "nonce_str")
    @Size(max = 32)
    private String nonceStr = Ids.oid();

    /**
     * 文件路径
     */
    @WftAlias(value = "filePath",sign = false)
    private String filePath;

    /**
     * 文件名称，如果不传默认为：账期.text
     */
    @WftAlias(value = "fileName",sign = false)
    private String fileName;

    /**
     * 文件后缀
     */
    @WftAlias(value = "fileSuffix",sign = false)
    private String fileSuffix = "txt";
}
