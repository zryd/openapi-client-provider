package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_ORDER_PAY,type = ApiMessageType.Request)
@XStreamAlias("REQUEST")
public class FuyouOrderPayRequest extends FuyouRequest{

    /**
     * 客户 IP
     * 客户所在 IP 地址
     */
    @XStreamAlias("USERIP")
    @NotBlank
    private String userIp;

    /**
     * 交易类型
     */
    @XStreamAlias("TYPE")
    @NotBlank
    private String type = "03";

    /**
     * 商户订单号
     * 商户订单流水号商户确保唯一
     */
    @Size(max = 60)
    @NotBlank
    @XStreamAlias("MCHNTORDERID")
    private String merchOrderNo;

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @XStreamAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    /**
     * 交易金额，分为单位
     */
    @XStreamAlias("AMT")
    @NotBlank
    @Size(max = 12)
    private String amount;

    /**
     * 协议号
     * 首次协议交易成功好生成的协议号
     */
    @XStreamAlias("PROTOCOLNO")
    @NotBlank
    @Size(max = 30)
    private String protocolNo;

    /**
     * 是否需要发送短信 固定值：0
     */
    @XStreamAlias("NEEDSENDMSG")
    @NotBlank
    private String isSmsCode = "0";


    /**
     * 保留字段 1
     */
    @XStreamAlias("REM1")
    @NotBlank
    @Size(max = 256)
    private String remOne;

    /**
     * 保留字段 2
     */
    @XStreamAlias("REM2")
    @NotBlank
    @Size(max = 256)
    private String remTwo;

    /**
     * 保留字段 3
     */
    @XStreamAlias("REM3")
    @NotBlank
    @Size(max = 256)
    private String remThree;

    /**
     * 后台通知URL
     * 商户接收支付结果后台通知地址
     */
    @XStreamAlias("BACKURL")
    @NotBlank
    @Size(max = 200)
    private String notifyUrl;

    @Override
    public String getSignStr() {
        StringBuilder signStr = new StringBuilder();
        signStr.append(getType()).append("|");
        signStr.append(getVersion()).append("|");
        signStr.append(getPartner()).append("|");
        signStr.append(getMerchOrderNo()).append("|");
        signStr.append(getUserId()).append("|");
        signStr.append(getProtocolNo()).append("|");
        signStr.append(getAmount()).append("|");
        signStr.append(getNotifyUrl()).append("|");
        signStr.append(getUserIp()).append("|");
        return signStr.toString();
    }
}
