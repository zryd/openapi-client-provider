package com.acooly.module.openapi.client.provider.yl.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yl.domain.YlApiMsgInfo;
import com.acooly.module.openapi.client.provider.yl.domain.YlRequest;
import com.acooly.module.openapi.client.provider.yl.enums.YlServiceEnum;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@YlApiMsgInfo(service = YlServiceEnum.YL_REAL_DEDUCT, type = ApiMessageType.Request)
public class YlRealDeductRequest extends YlRequest {

    /**
     * 记录序号  (同一个请求内必须唯一。从0001开始递增)
     */
    private String sn;

    /**
     * 总金额(整数，单位分)
     */
    private String totalSum;

    /**
     * 业务代码
     */
    private String businessCode;


    /** 协议号 */
    private String protocolNo;

    /**
     * 商户代码
     */
    private String merchantId;
    /**
     * 提交时间(YYYYMMDDHHMMSS) 可空
     */
    private String submitTime;

    /**
     * 总记录数:固定为1
     */
    private String totalItem = "1";

    /**
     * 客户与商户签约并在银联网络备案的客户编号 可空
     */
    private String eUserCode;

    /**
     *  银行代码    (3-8位的银行代码)
     */
    private String bankCode;

    /**
     * 账号类型  (00银行卡，01存折  可空)
     */
    private String accountType = "00";
    /**
     * 账号 (银行卡或存折号码)
     */
    private String accountNo;
    /**
     * 账号名   (客户姓名)
     */
    private String accountName;
    /**
     * 开户行所在省 可空
     */
    private String province;
    /**
     * 开户行所在市  可空
     */
    private String city;
    /**
     * 开户行名称(开户行详细名称    可空)
     */
    private String bankName;
    /**
     * 账号属性    ( 0私人，1公司。不填时，默认为私人0   可空)
     */
    private String accountProp;
    /**
     * 金额 (整数，单位分)
     */
    private String amount;
    /**
     * 货币类型 ( 默认为 人民币：CNY 可空)
     */
    private String currency;
    /**
     * 协议号 可空
     */
    private String protocol;
    /**
     * 协议用户编号 可空
     */
    private String protocolUserid;
    /**
     * 开户证件类型  0：身份证    可空
     */
    private String idType;
    /**
     * 证件号 可空
     */
    private String id;
    /**
     * 手机号/小灵通  可空
     */
    private String tel;
    /**
     * 资金清算账号 可空
     */
    private String reckonAccount;
    /**
     * 商户自定义的用户号， 用户号要唯一 可空
     */
    private String custUserid;
    /**
     * 备注 供商户填入参考信息
     */
    private String remark;

    /**
     * 电子联行号  可空
     */
    private String eleBankno;
    /**
     * 摘要 可空
     */
    private String abs;
    /**
     * 附言 可空
     */
    private String ps;
    /**
     * 用途 可空
     */
    private String use;
    /**
     * 信用卡有效期  (YYMM) 可空
     */
    private String creValDate;
    /**
     * CVN2码 可空
     */
    private String creCvn2;
    /**
     * 账号密码 可空
     */
    private String accPass;

}
