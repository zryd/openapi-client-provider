package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankRequest;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBalancePayQueryRequestInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.BALANCE_PAY_QUERY, type = ApiMessageType.Request)
public class WsbankBalancePayQueryRequest extends WsbankRequest {

    @XStreamAlias("request")
    private WsbankBalancePayQueryRequestInfo wsbankBalancePayQueryRequestInfo;

    @Override
    public void doCheck() {
        Validators.assertJSR303(wsbankBalancePayQueryRequestInfo);
        Validators.assertJSR303(wsbankBalancePayQueryRequestInfo.getHeadRequest());
        Validators.assertJSR303(wsbankBalancePayQueryRequestInfo.getWsbankBalancePayQueryRequestBody());
    }
}
