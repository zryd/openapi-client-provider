package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/25 14:54
 */
@Getter
@Setter
@XStreamAlias("response")
public class WsbankNotifyResponseInfo implements Serializable {
    /**
     * 响应报文头
     */
    @XStreamAlias("head")
    private WsbankHeadResponse headResponse;

    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    private WsbankNotifyResponseBody responseBody;
}
