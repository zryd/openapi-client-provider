package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankOrderShareQueryResponseBody implements Serializable {

	private static final long serialVersionUID = 6918587792025188970L;

	/**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;
    
	/**
	 * 关联网商订单号
	 */
    @XStreamAlias("RelateOrderNo")
    private String relateOrderNo;

    /**
     * 外部订单分账请求流水号
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;

    /**
     * 分账单号
     */
    @XStreamAlias("ShareOrderNo")
    private String shareOrderNo;

    /**
     * 订单金额
     */
    @XStreamAlias("TotalAmount")
    private long totalAmount;

    /**
     * 币种
     */
    @XStreamAlias("Currency")
    private String currency;

    /**
     * 分账时间
     */
    @XStreamAlias("ShareDate")
    private String shareDate;
    
    /**
     * 状态
     */
    @XStreamAlias("Status")
    private String status;
    
    /**
     * 错误码
     */
    @XStreamAlias("ErrorCode")
    private String errorCode;
    
    /**
     * 错误描述
     */
    @XStreamAlias("ErrorDesc")
    private String errorDesc;
    
    /**
     * 扩展信息
     */
    @XStreamAlias("ExtInfo")
    private String extInfo;
    
    /**
     * 备注
     */
    @XStreamAlias("Memo")
    private String memo;

}
