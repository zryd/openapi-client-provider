package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankResponse;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankRegisterQueryResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author WEILI 2018/5/22 15:32
 */
@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.REGISTER_QUERY,type = ApiMessageType.Response)
public class WsbankRegisterQueryResponse extends WsbankResponse {

    /**
     * 响应报文信息
     */
    @XStreamAlias("response")
    private WsbankRegisterQueryResponseInfo responseInfo;
}
