/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-24 16:11 创建
 */
package com.acooly.module.openapi.client.provider.shengpay.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.shengpay.ShengpayConstants;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 跳转请求报文组装
 *
 * @author zhike 2017-09-24 16:11
 */
@Slf4j
@Component
public class ShengpayRedirectPostMarshall extends ShengpayAbstractMarshall implements ApiMarshal<PostRedirect, ShengpayRequest> {

    @Override
    public PostRedirect marshal(ShengpayRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        postRedirect.setRedirectUrl(ShengpayConstants.getCanonicalUrl(getProperties().getGatewayUrl(),
                ShengpayConstants.getServiceName(source)));
//        postRedirect.addData(ShengpayConstants.REQUEST_PARAM_NAME, doMarshall(source));
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }


}