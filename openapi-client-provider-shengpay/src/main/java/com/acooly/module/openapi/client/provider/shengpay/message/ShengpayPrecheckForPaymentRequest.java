package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayRequest;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayBankCardTypeEnum;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayCardTypeEnum;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/** @author zhike 2018/5/17 18:54 */
@Getter
@Setter
@ShengpayApiMsg(
  service = ShengpayServiceNameEnum.PRECHECK_FOR_PAYMENT,
  type = ApiMessageType.Request
)
public class ShengpayPrecheckForPaymentRequest extends ShengpayRequest {

    /** 支付token，成功创建支付订单之后返回 */
    private String sessionToken;

    /** 绑卡协议号，首次签约成功之后盛付通系统返回的协议号 */
    private String agreementNo;

    /**
     * 是否重发短信标识，默认为false false：表示首次调支付预校验
     * true：表示首次调完支付预校验之后，用户没收到短信，或者收到短信超时，此时商户可设置为true，在同一会话内再次请求进行重发短信操作
     */
    private String isResendValidateCode;

    /** 商户系统内针对会员的唯一标识，不同会员的标识必须唯一 */
    private String outMemberId;

    /**
     * 机构代码，如：ICBC
     * 参考枚举：com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayBankCodeEnum
     */
    @NotBlank
    private String bankCode;

    /**
     * 银行卡类型，如：CR/DR
     */
    @NotBlank
    private String bankCardType = ShengpayBankCardTypeEnum.BANK_CARD_TYPE_DR.getCode();

    /**
     * 银行卡明文
     */
    @NotBlank
    private String bankCardNo;

    /**
     * 用户真实姓名，应与银行卡账户姓名保持一致
     */
    @NotBlank
    private String realName;

    /**
     * 证件号码, 与银行预留一致
     */
    @NotBlank
    private String idNo;

    /**
     * 证件类型，如：IC
     */
    @NotBlank
    private String idType = ShengpayCardTypeEnum.CARD_TYPE_IC.getCode();

    /**
     * 银行预留手机号
     */
    @NotBlank
    private String mobileNo;

    /**
     * 信用卡CVV2, 信用卡首次支付时该字段必填
     */
    private String cvv2;

    /**
     * 信用卡有效期，格式：yy/mm， 信用卡首次支付时必填
     */
    private String validThru;

    /**
     * 用户IP（用户支付时的IP）
     */
    @NotBlank
    private String userIp;

    /**
     * 风控扩展属性, JSON串，{ "outMemberId":"商户系统内针对会员的唯一标识,跟输入参数outMemberId传值一样",
     * "outMemberRegistTime":"商户会员注册时间yyyyMMddHHmmss,如:20110707112233",
     * "outMemberRegistIP":"商户会员注册IP", "outMemberVerifyStatus":"商户会员是否已实名(1实名0未实名)",
     * "outMemberName":"商户会员注册姓名", "outMemberMobile":"商户会员注册手机号" } 说明：要求的每项必须传
     */
    private String riskExtItems;
}
