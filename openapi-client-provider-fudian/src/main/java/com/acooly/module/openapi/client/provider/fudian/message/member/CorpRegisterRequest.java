/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-03 00:41:43 创建
 */
package com.acooly.module.openapi.client.provider.fudian.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianRequest;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu 2018-02-03 00:41:43
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.CORP_REGISTER, type = ApiMessageType.Request)
public class CorpRegisterRequest extends FudianRequest {


}