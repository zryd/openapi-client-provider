/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-03 00:47:05 创建
 */
package com.acooly.module.openapi.client.provider.fudian.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-03 00:47:05
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.CORP_REGISTER, type = ApiMessageType.Response)
public class CorpRegisterResponse extends FudianResponse {

    /**
     * 法人代表证件号
     * 证件类型为身份证时，仅支持18位长度
     */
    @NotEmpty
    @Length(min = 18, max = 18)
    private String artificialIdentityCode;

    /**
     * 法人代表真实姓名
     * 企业法人的真实姓名
     */
    @NotEmpty
    @Length(max = 60)
    private String artificialRealName;

    /**
     * 企业名称
     * 企业名称
     */
    @NotEmpty
    @Length(max = 256)
    private String corpName;

    /**
     * 公司类型
     * 1：借款企业；2：担保公司 3 出借企业
     */
    @NotEmpty
    @Length(min = 1, max = 1)
    private String corpType;

    /**
     * 统一社会信用代码
     * 统一社会信用代码
     */
    @NotEmpty
    @Length(max = 256)
    private String creditCode;


    /**
     * 营业执照编号
     * 营业执照编号
     */
    @NotEmpty
    @Length(max = 30)
    private String licenceCode;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max = 8)
    private String merchantNo;

    /**
     * 法人手机号码
     * 联系人手机号
     */
    @NotEmpty
    @Length(min = 11, max = 11)
    private String mobilePhone;

    /**
     * 异步回调地址
     * 服务器通知业务参数，请根据此做业务处理
     */
    @NotEmpty
    @Length(max = 256)
    private String notifyUrl;


    /**
     * 组织机构代码
     * 组织机构代码
     */
    @NotEmpty
    @Length(min = 30, max = 30)
    private String orgCode;

    /**
     * 税务登记号
     * 税务登记号
     */
    @NotEmpty
    @Length(max = 30)
    private String taxRegCode;

    /**
     * 是否三证合一
     * 0否;1是
     */
    @NotEmpty
    @Length(min = 1, max = 1)
    private String threeCertUnit;
}